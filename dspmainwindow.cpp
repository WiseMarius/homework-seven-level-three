#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>
#include <aquila\aquila.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);

	ui->plot->xAxis->setRange(0, 0.01);
	ui->plot->yAxis->setRange(-300, 300);

	initSlots();
	initLineEditValidator();
}

void DSPMainWindow::plotWave(QVector<double>& xAxis, QVector<double>& yAxis)
{
	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxis);
	ui->plot->replot();
}

void DSPMainWindow::createInformationSignal(double samplingRate, double frequency, QVector<double>& yAxis, QVector<double>& xAxis)
{
	Aquila::TriangleGenerator generator(samplingRate);
	generator.setFrequency(frequency / 100).setAmplitude(16).generate(samplingRate * 0.01);

	for (double index = 0; index < samplingRate; index++)
	{
		xAxis[index] = index / samplingRate;
	}

	for (std::size_t i = 0; i < generator.getSamplesCount(); ++i)
	{
		yAxis[i] = generator.sample(i);
	}
}

void DSPMainWindow::createCarrierSignal(double samplingRate, double frequency, QVector<double>& yAxis, QVector<double>& xAxis)
{
	Aquila::SineGenerator generator(samplingRate);
	generator.setFrequency(frequency / 100).setAmplitude(16).generate(samplingRate * 0.01);

	for (double index = 0; index < samplingRate; index++)
	{
		xAxis[index] = index / samplingRate;
	}

	for (std::size_t i = 0; i < generator.getSamplesCount(); ++i)
	{
		yAxis[i] = generator.sample(i);
	}
}

void DSPMainWindow::createAMSignal(double frequency)
{
	const double sampleRate = 44100.0;

	QVector<double> xAxis(sampleRate);
	QVector<double> yAxisInformation(sampleRate);
	QVector<double> yAxisCarrier(sampleRate);
	QVector<double> yAxisAM(sampleRate);

	createInformationSignal(sampleRate, frequency, yAxisInformation, xAxis);
	createCarrierSignal(sampleRate, frequency, yAxisCarrier, xAxis);

	for (double index = 0; index < sampleRate; index++)
	{
		yAxisAM[index] = (1 + yAxisInformation[index]) * yAxisCarrier[index];
	}

	plotWave(xAxis, yAxisAM);
}

void DSPMainWindow::mf_PlotButton()
{
	createAMSignal(ui->frequencyLineEdit->text().toDouble() * 100);
}

void DSPMainWindow::initSlots()
{
	QObject::connect(ui->plotPushButton, SIGNAL(released()), this, SLOT(mf_PlotButton()));
}

void DSPMainWindow::initLineEditValidator()
{
	ui->frequencyLineEdit->setValidator(new QIntValidator(this));
}

DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}