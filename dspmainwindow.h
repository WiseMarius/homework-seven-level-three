#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DSPMainWindow(QWidget *parent = 0);
    ~DSPMainWindow();

private:
    Ui::DSPMainWindow *ui;
	void plotWave(QVector<double>& xAxis, QVector<double>& yAxis);
	void initSlots();
	void initLineEditValidator();
	void createInformationSignal(double, double, QVector<double>&, QVector<double>&);
	void createCarrierSignal(double samplingRate, double frequency, QVector<double>& yAxis, QVector<double>& xAxis);
	void createAMSignal(double);

	private slots:
	void mf_PlotButton();
};

#endif // DSPMAINWINDOW_H
